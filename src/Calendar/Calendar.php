<?php

namespace Dj\Wikitest\Calendar;

class Calendar 
{
    /**
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * @var WorkingHours
     */
    private $workingHours;

    /**
     * @var TimeSlot[]
     */
    private $busySlots = [];

    /**
     * @var string
     */
    private $ownerName;

    /**
     * @param WorkingHours $workingHours
     * @param \DateTimeZone $timeZone
     */
    public function __construct(WorkingHours $workingHours = null, \DateTimeZone $timeZone = null)
    {
        $this->workingHours = $workingHours;
        $this->timeZone = $timeZone;
    }

    /**
     * @param TimeSlot $slot
     * @return bool
     */
    public function hasFreeSlot(TimeSlot $slot)
    {
        if (!$this->workingHours->containsSlot($slot, $this->timeZone)) {
            return false;
        }

        return !$this->isBusy($slot);
    }

    /**
     * @param TimeSlot $timeSlot
     */
    public function addBusyTimeSlot(TimeSlot $timeSlot)
    {
        $this->busySlots[] = $timeSlot;
    }

    /**
     * @param TimeSlot $slot
     * @return bool
     */
    private function isBusy(TimeSlot $slot)
    {
        foreach ($this->busySlots as $busySlot) {
            if ($busySlot->overlapWith($slot)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $name
     */
    public function setOwnerName($name)
    {
        $this->ownerName = $name;
    }

    /**
     * @return string
     */
    public function getOwnerName()
    {
        return $this->ownerName;
    }
}