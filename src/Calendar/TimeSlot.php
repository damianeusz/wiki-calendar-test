<?php

namespace Dj\Wikitest\Calendar;

class TimeSlot 
{
    /**
     * @var \DateTime
     */
    private $startAt;

    /**
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * @var \DateTime
     */
    private $endAt;

    /**
     * @param \DateTime $startAt
     * @param \DateInterval $length
     */
    public function __construct(\DateTime $startAt, \DateInterval $length)
    {
        $this->startAt = $startAt;

        $endAt = clone($startAt);
        $endAt->add($length);
        $this->endAt = $endAt;
    }

    /**
     * @return \DateTime
     */
    public function getStartAt()
    {
        return $this->startAt;
    }

    /**
     * @return \DateTime
     */
    public function getEndAt()
    {
        return $this->endAt;
    }

    /**
     * @return \DateTimeZone
     */
    public function getTimeZone()
    {
        return $this->timeZone;
    }

    /**
     * @param TimeSlot $slot
     * @return bool
     */
    public function overlapWith(TimeSlot $slot)
    {
        return $this->startAt < $slot->getEndAt() and $slot->getStartAt() < $this->endAt;
    }
}