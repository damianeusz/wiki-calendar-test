<?php

namespace Dj\Wikitest\Calendar;

class WorkingHours 
{
    /**
     * @var string
     */
    private $startAt;

    /**
     * @var int
     */
    private $hoursNumber;

    /**
     * @param string $startAt
     * @param int $hoursNumber
     */
    public function __construct($startAt, $hoursNumber)
    {
        $this->startAt = $startAt;
        $this->hoursNumber = $hoursNumber;
    }

    /**
     * @param TimeSlot $slot
     * @param \DateTimeZone $timeZone
     * @return bool
     */
    public function containsSlot(TimeSlot $slot, \DateTimeZone $timeZone)
    {
        $dayStartAt = new \DateTime($slot->getStartAt()->format('Y-m-d') . ' ' . $this->startAt, $timeZone);
        if ($dayStartAt > $slot->getStartAt()) {
            return false;
        }

        $dayEndAt = $dayStartAt->add(new \DateInterval('PT' . $this->hoursNumber . 'H'));
        if ($dayEndAt < $slot->getEndAt()) {
            return false;
        }

        return true;
    }
}