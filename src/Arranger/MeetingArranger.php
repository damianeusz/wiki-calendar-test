<?php

namespace Dj\Wikitest\Arranger;

use Dj\Wikitest\Calendar\Calendar;
use Dj\Wikitest\Calendar\DateRange;
use Dj\Wikitest\Calendar\TimeSlot;

class MeetingArranger
{
    /**
     * Distance between possible slots starts in minutes
     */
    const SLOT_INTERVAL = 30;

    /**
     * @var Calendar[]
     */
    private $calendars;

    /**
     * @var \DateTimeZone
     */
    private $timeZone;

    /**
     * @param \DateTimeZone $timeZone
     */
    public function __construct(\DateTimeZone $timeZone)
    {
        $this->timeZone = $timeZone;
    }

    /**
     * @param Calendar[] $calendars
     */
    public function setCalendars(array $calendars)
    {
        $this->calendars = $calendars;
    }

    /**
     * @return Calendar[]
     */
    public function getCalendars()
    {
        return $this->calendars;
    }

    /**
     * @param DateRange $searchRange
     * @param int $lengthInMinutes
     * @param null|int $maxSlots
     * @return array
     * @throws NoSlotsAvailableException
     */
    public function findAvailableSlots(DateRange $searchRange, $lengthInMinutes, $maxSlots = null)
    {
        $map = $this->mapSlotsAvailability($searchRange->getFrom(), $searchRange->getTo(), $lengthInMinutes);

        $availableSlots = $this->selectAvailableSlots($map, $lengthInMinutes, $maxSlots);

        if (empty($availableSlots)) {
            throw new NoSlotsAvailableException("It's not possible arrange meeting with everyone in the selected time-frame");
        }

        return $availableSlots;
    }

    /**
     * @param DateRange $searchRange
     * @param int $lengthInMinutes
     * @return MaxParticipationResponse
     */
    public function findSlotWithMaxParticipation(DateRange $searchRange, $lengthInMinutes)
    {
        $map = $this->mapSlotsAvailability($searchRange->getFrom(), $searchRange->getTo(), $lengthInMinutes);

        $bestSlot = $this->selectMaxAvailableSlot($map, $lengthInMinutes);

        $participation = new MaxParticipationResponse($bestSlot);

        foreach ($this->calendars as $calendar) {
            if ($calendar->hasFreeSlot($bestSlot)) {
                $participation->addAvailableAttendee($calendar->getOwnerName());
            } else {
                $participation->addUnavailableAttendee($calendar->getOwnerName());
            }
        }

        return $participation;
    }

    /**
     * @param TimeSlot $timeSlot
     * @return int
     */
    private function calculateSlotAvailability(TimeSlot $timeSlot)
    {
        $count = 0;
        foreach ($this->calendars as $calendar) {
            if ($calendar->hasFreeSlot($timeSlot)) {
                $count ++;
            }
        }
        return $count;
    }

    /**
     * @param \DateTime $periodBegin
     * @param \DateTime $periodEnd
     * @param $lengthInMinutes
     * @return array
     */
    private function mapSlotsAvailability(
        \DateTime $periodBegin,
        \DateTime $periodEnd,
        $lengthInMinutes
    ) {
        $map = [];
        $firstSlotStartAt = $this->nearestSlot($periodBegin);

        $interval = new \DateInterval('PT'.self::SLOT_INTERVAL.'M');
        $periodIterator = new \DatePeriod($firstSlotStartAt, $interval, $periodEnd);

        foreach($periodIterator as $slotStartAt){
            $timeSlot = $this->createTimeSlot($slotStartAt, $this->timeZone, $lengthInMinutes);
            $slotAvailability = $this->calculateSlotAvailability($timeSlot);
            $map[$slotStartAt->format('Y-m-d H:i')] = $slotAvailability;
        }

        return $map;
    }

    /**
     * @param array $map
     * @param int $lengthInMinutes
     * @param null|int $maxSlots
     * @return array
     */
    private function selectAvailableSlots(array & $map, $lengthInMinutes, $maxSlots = null)
    {
        $slots = [];
        foreach ($map as $timeStr => $slotAvailability) {
            if ($slotAvailability === count($this->calendars)) {
                $slots[] = $this->createTimeSlot(new \DateTime($timeStr), $this->timeZone, $lengthInMinutes);
            }
            if ($maxSlots !== null && count($slots) === $maxSlots) {
                break;
            }
        }

        return $slots;
    }

    /**
     * @param \DateTime $time
     * @return \DateTime
     */
    private function nearestSlot(\DateTime $time)
    {
        $slot = clone($time);
        $toFix = (int)$slot->format('i') % self::SLOT_INTERVAL;
        if ($toFix > 0) {
            $fix = self::SLOT_INTERVAL - $toFix;
            $slot->add(new \DateInterval('PT' . $fix . 'M'));
        }

        return $slot;
    }

    /**
     * @param array $map
     * @param int $lengthInMinutes
     * @return TimeSlot
     */
    private function selectMaxAvailableSlot(array & $map, $lengthInMinutes)
    {
        $timeStr = array_keys($map, max($map))[0];

        $bestSlot = $this->createTimeSlot(new \DateTime($timeStr), $this->timeZone, $lengthInMinutes);
        return $bestSlot;
    }

    /**
     * @param \DateTime $slotStartAt
     * @param \DateTimeZone $timeZone
     * @param int $lengthInMinutes
     * @return TimeSlot
     */
    private function createTimeSlot(\DateTime $slotStartAt, \DateTimeZone $timeZone, $lengthInMinutes)
    {
        return new TimeSlot($slotStartAt, new \DateInterval("PT{$lengthInMinutes}M"), $timeZone);
    }
}