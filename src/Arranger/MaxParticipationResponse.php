<?php

namespace Dj\Wikitest\Arranger;

use Dj\Wikitest\Calendar\TimeSlot;

class MaxParticipationResponse
{
    /**
     * @var TimeSlot
     */
    private $timeSlot;
    /**
     * @var array
     */
    private $availableAttendees = [];
    /**
     * @var array
     */
    private $unavailableAttendees = [];

    /**
     * @param TimeSlot $timeSlot
     */
    public function __construct(TimeSlot $timeSlot)
    {
        $this->timeSlot = $timeSlot;
    }

    /**
     * @param array|string[] $availableAttendees
     * @param array|string[] $unavailableAttendees
     */
    public function setAttendees(array $availableAttendees, array $unavailableAttendees)
    {
        $this->availableAttendees = $availableAttendees;
        $this->unavailableAttendees = $unavailableAttendees;
    }

    /**
     * @param string $attendeeName
     */
    public function addAvailableAttendee($attendeeName)
    {
        $this->availableAttendees[] = $attendeeName;
    }

    /**
     * @param string  $attendeeName
     */
    public function addUnavailableAttendee($attendeeName)
    {
        $this->unavailableAttendees[] = $attendeeName;
    }
}