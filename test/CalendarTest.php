<?php

namespace Dj\Wikitest;

use Dj\Wikitest\Calendar\Calendar;
use Dj\Wikitest\Calendar\TimeSlot;
use Dj\Wikitest\Calendar\WorkingHours;

class CalendarTest extends \PHPUnit_Framework_TestCase
{
    public function provideWorkingHoursCases()
    {
        return [
            ['8:00', 10, $this->halfHourSlot('2009-11-12 09:00'), true],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 08:00'), true],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 17:30'), true],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 07:30'), false],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 18:30'), false],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 21:30'), false],
            ['8:00', 10, $this->halfHourSlot('2009-11-12 11:00', 'America/New_York'), true],
            ['8:00', 8, $this->halfHourSlot('2009-11-12 13:00', 'America/New_York'), false],
        ];
    }

    /**
     * @test
     * @dataProvider provideWorkingHoursCases
     */
    public function should_has_free_slot_between_working_hours($startAt, $hoursNumber, $timeSlot, $result)
    {
        $calendar = new Calendar(new WorkingHours($startAt, $hoursNumber), new \DateTimeZone('Europe/Warsaw'));

        $this->assertEquals($result, $calendar->hasFreeSlot($timeSlot));
    }

    /**
     * @test
     */
    public function should_has_free_slot_between_busy_slots()
    {
        $calendar = new Calendar(new WorkingHours('08:00', 8), new \DateTimeZone('Europe/Warsaw'));
        $calendar->addBusyTimeSlot($this->halfHourSlot('2009-11-12 09:30'));

        $timeSlot = $this->halfHourSlot('2009-11-12 09:00');
        $this->assertTrue($calendar->hasFreeSlot($timeSlot));
    }

    private function halfHourSlot($time, $timeZone = 'Europe/Warsaw')
    {
        return new TimeSlot(new \DateTime($time, new \DateTimeZone($timeZone)), new \DateInterval('PT30M'));
    }
}
 