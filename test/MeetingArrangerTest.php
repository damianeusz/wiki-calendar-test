<?php

namespace Dj\Wikitest;

use Dj\Wikitest\Arranger\MaxParticipationResponse;
use Dj\Wikitest\Arranger\MeetingArranger;
use Dj\Wikitest\Calendar\Calendar;
use Dj\Wikitest\Calendar\DateRange;
use Dj\Wikitest\Calendar\TimeSlot;
use Dj\Wikitest\Calendar\WorkingHours;

class MeetingArrangerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var \DateTimeZone
     */
    private $timeZone;
    /**
     * @var MeetingArranger
     */
    private $arranger;

    public function setUp()
    {
        $this->timeZone = new \DateTimeZone('Europe/Warsaw');
        $this->arranger = new MeetingArranger($this->timeZone);
    }

    /**
     * @test
     */
    public function should_find_time_slots_for_attendees_with_different_working_hours()
    {
        $this->arranger->setCalendars(
            [
                $this->calendar('8:00', 8, $this->timeZone),
                $this->calendar('10:00', 8, $this->timeZone),
                $this->calendar('6:00', 8, $this->timeZone),
            ]
        );

        $this->assertEquals(
            [
                $this->timeSlot('2008-11-12 10:00', $this->timeZone, '240M'),
                $this->timeSlot('2008-11-13 10:00', $this->timeZone, '240M'),
            ],
            $this->arranger->findAvailableSlots(
                $this->dateRange('2008-11-12 00:15', '2008-11-14 00:00', $this->timeZone),
                240,
                $this->timeZone
            )
        );
    }

    /**
     * @test
     */
    public function should_find_time_slots_for_attendees_with_different_timeZones()
    {
        $this->arranger->setCalendars(
            [
                $this->calendar('9:00', 8, new \DateTimeZone('America/New_York')),//-5
                $this->calendar('8:00', 8, new \DateTimeZone('Europe/Lisbon')),//+0
                $this->calendar('10:00', 8, new \DateTimeZone('Europe/Warsaw')),//+1
            ]
        );

        $this->assertEquals(
            [
                $this->timeSlot('2008-11-12 15:00', $this->timeZone, '120M'),
                $this->timeSlot('2008-11-13 15:00', $this->timeZone, '120M'),
            ],
            $this->arranger->findAvailableSlots(
                $this->dateRange('2008-11-12 00:00', '2008-11-14 00:00', $this->timeZone),
                120
            )
        );
    }

    /**
     * @test
     */
    public function should_find_free_slots_for_attendees_with_busy_slots()
    {
        $calendar1 = $this->calendar('10:00', 8, $this->timeZone);
        $calendar1->addBusyTimeSlot($this->timeSlot('2008-11-12 15:00', $this->timeZone, '120M'));
        $calendar2 = $this->calendar('10:00', 8, $this->timeZone);
        $calendar2->addBusyTimeSlot($this->timeSlot('2008-11-12 11:00', $this->timeZone, '120M'));
        $this->arranger->setCalendars([$calendar1, $calendar2]);

        $this->assertEquals(
            [
                $this->timeSlot('2008-11-12 13:00', $this->timeZone, '120M'),
            ],
            $this->arranger->findAvailableSlots(
                $this->dateRange('2008-11-12 00:00', '2008-11-13 00:00', $this->timeZone),
                120
            )
        );
    }

    /**
     * @test
     */
    public function should_find_max_given_number_of_slots()
    {
        $this->arranger->setCalendars([
                $this->calendar('10:00', 3, $this->timeZone)
            ]);

        $this->assertEquals(
            [
                $this->timeSlot('2008-11-12 10:00', $this->timeZone, '120M'),
                $this->timeSlot('2008-11-12 10:30', $this->timeZone, '120M'),
            ],
            $this->arranger->findAvailableSlots(
                $this->dateRange('2008-11-12 00:00', '2008-11-13 00:00', $this->timeZone),
                120,
                2
            )
        );
    }

    /**
     * @test
     * @expectedException \Dj\Wikitest\Arranger\NoSlotsAvailableException
     */
    public function should_throw_exception_when_cannot_find_free_slot()
    {
        $calendar = $this->calendar('10:00', 2, $this->timeZone);
        $calendar->addBusyTimeSlot($this->timeSlot('2008-11-12 10:00', $this->timeZone, '120M'));

        $this->arranger->setCalendars([$calendar]);

        $this->arranger->findAvailableSlots(
            $this->dateRange('2008-11-12 00:00', '2008-11-13 00:00', $this->timeZone),
            120
        );
    }

    /**
     * BONUS
     *
     * @test
     */
    public function should_find_slot_with_max_participation()
    {
        $calendar1 = $this->calendar('10:00', 8, $this->timeZone, 'Ben');
        $calendar1->addBusyTimeSlot($this->timeSlot('2008-11-12 10:00', $this->timeZone, '8H'));
        $calendar2 = $this->calendar('10:00', 5, $this->timeZone, 'Liz');
        $calendar3 = $this->calendar('13:00', 5, $this->timeZone, 'Ken');
        $this->arranger->setCalendars([$calendar1, $calendar2, $calendar3]);

        $expected = new MaxParticipationResponse(
            $this->timeSlot('2008-11-12 13:00', $this->timeZone, '120M')
        );
        $expected->setAttendees(['Liz', 'Ken'], ['Ben']);

        $this->assertEquals(
            $expected,
            $this->arranger->findSlotWithMaxParticipation(
                $this->dateRange('2008-11-12 00:00', '2008-11-13 00:00', $this->timeZone),
                120
            )
        );
    }

    /**
     * @param string $timeStr
     * @param \DateTimeZone $timeZone
     * @param string $length
     * @return TimeSlot
     */
    private function timeSlot($timeStr, \DateTimeZone $timeZone, $length)
    {
        return new TimeSlot(new \DateTime($timeStr, $timeZone), new \DateInterval('PT' . $length));
    }

    /**
     * @param string $from
     * @param string $to
     * @param \DateTimeZone $timeZone
     * @return DateRange
     */
    private function dateRange($from, $to, \DateTimeZone $timeZone)
    {
        return new DateRange(
            new \DateTime($from, $timeZone),
            new \DateTime($to, $timeZone)
        );
    }

    /**
     * @param $start
     * @param $hours
     * @param \DateTimeZone $timeZone
     * @param $ownerName
     * @return Calendar
     */
    private function calendar($start, $hours, \DateTimeZone $timeZone, $ownerName = null)
    {
        $calendar = new Calendar(new WorkingHours($start, $hours), $timeZone);
        $calendar->setOwnerName($ownerName);

        return $calendar;
    }
}
 